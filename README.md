# README #

Ola Leo, fiz um exemplo lendo o conteudo da pagina da 
previdencia para vc capturar os dados das tabelas


### What is this repository for? ###

* Quick summary
Uso das bibliotecas urllib e BeautifulSoup para fazer scraping de uma pagina web
* Version
1.0
* [Learn Markdown](https://samirjahchan@bitbucket.org/samirjahchan/previdencia.git)

### How do I get set up? ###

* Summary of set up
Clonar o repositorio
git clone https://samirjahchan@bitbucket.org/samirjahchan/previdencia.git

* Configuration
Ativar o ambiente virtual
$cd previdencia
<previdencia>$source ./venv/bin/activate

* Dependencies
Instalar a biblioteca do BeautifulSoup
<previdencia>$python -m pip install requirements.txt
