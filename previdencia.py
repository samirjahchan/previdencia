# -*- utf-8 -*-
from urllib.request import urlopen
from bs4 import BeautifulSoup

url = "http://www.previdencia.gov.br/servicos-ao-cidadao/todos-os-servicos/gps/tabela-contribuicao-mensal/"
page = urlopen(url)
soup = BeautifulSoup(page, "html.parser")

tabela = []
tables = soup.find_all('table')
for table in tables:
    table_body = table.find('tbody')

    rows = table_body.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        tabela.append([ele for ele in cols if ele]) # Get rid of empty values


for lista in tabela:
    print(lista[0], lista[1])
